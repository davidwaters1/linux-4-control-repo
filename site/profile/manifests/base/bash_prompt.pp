# Class: profile::base::bash_prompt
#
#
class profile::base::bash_prompt {
  file { '/etc/profile.d/puppet_bashprompt.sh':
    ensure  => file,
    content => 'export PS1="\[\033[48;5;9m\][\u@\h]\\$\[$(tput sgr0)\]"',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }
}
